# _Personalized Letter_

### _form that personalizes a letter for the user, August 10, 2016_

#### _**By Aimen Khakwani and Jena SanCartier**_

## Description

_This simple app takes user input and generates a personalized letter!._

##Setup and Installation

* _Clone_
* _Run in browser_

## Technologies Used

_HTML, CSS, Bootstrap, jQuery, and JavaScript_

### License
Copyright (c) 2016 **_Aimen Khakwani & Jena SanCartier_**
